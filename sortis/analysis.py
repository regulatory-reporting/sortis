import pandas as pd 

# Import sortis package
from sortis.classifier import SentenceClassifier
from sortis.dependency_tree import DependencyTree
from sortis.mapping import extract_agents, add_agent, add_request, add_action, add_action_result, add_agent_role, add_date, instantiating_roles

# Import other packages and models
import rdflib
from rdflib.namespace import Namespace
import spacy
import json

RRMV = Namespace("http://data.europa.eu/2qy/rrmv#", )
CCCEV = Namespace("https://semiceu.github.io/CCCEV#")
ELI = Namespace("http://data.europa.eu/eli/ontology#")
LFDS = Namespace("http://data.europa.eu/2qy/lfds#", )


# Load spacy model
# spacy.cli.download("en_core_web_lg")
spacy_model = spacy.load('en_core_web_lg')
spacy_model.add_pipe("merge_noun_chunks")

class LegalAnalysis:
    def __init__(self):
        pass

    def analyse(self, parser, celex, analysis_type='ReportingRequirement'):        
        if analysis_type not in self.analysis_types:
            raise ValueError(f"Invalid analysis type: {analysis_type}. Supported types are: {list(self.analysis_types.keys())}")
        analysis = self.analysis_types[analysis_type](self)
        analysis.analyse(parser, celex)
        
    def initialise_graph(self):
        self.g = rdflib.Graph()

        # Bind graph to namespaces
        self.g.bind("rrmv", RRMV)
        self.g.bind("cccev", CCCEV)
        self.g.bind("eli", ELI)
        self.g = instantiating_roles(self.g)

class ReportingRequirementAnalysis(LegalAnalysis):
    def __init__(self):
        super().__init__()
        self.initialise_graph()
            
    def analyse(self, parser, celex):
        self.celex = celex
        self.parser = parser                
        self.get_sentences()
        self.detect_requirements()
        self.find_requests()        
        self.populate_graph()


    def get_sentences(self):        
        # Initialize an empty list to store data
        id = 1
        data = []
        for article in self.parser.articles:
            for paragraph in article['article_text']:
                # If within the first 2 character are a number followed by a point, remove these characters and trim spaces.
                paragraph['text'] = paragraph['text'].lstrip('0123456789.').strip()                
                
                # Process the text with the Spacy model to split it into sentences
                doc = spacy_model(paragraph['text'])
                # Split the text into sentences
                sentences = [sentence.text for sentence in doc.sents]
                # Iterate over the sentences to create a new dataframe
                for i, s in enumerate(sentences):
                    if s.strip():  # Check if the sentence is not empty                                                
                        # Create a dictionary for each text and append it to the data list
                        data.append({
                            'celex': self.celex,
                            'id': id,
                            'eId': paragraph['eId'],
                            'sentence': s.strip()
                        })
                        id += 1       
        self.sentences = pd.DataFrame(data)        
        #print(self.sentences)

    def detect_requirements(self):
        # Classify using SORTIS Classifier package to identify reporting requirements
        self.requirements = pd.DataFrame()
        self.sentences['sentence_type'] = self.sentences['sentence'].apply(lambda x: "OBLIGATION" if SentenceClassifier(sentence=x).requirement else "OTHER")        
        self.actions = self.sentences[self.sentences['sentence_type'] == 'OBLIGATION']        

    def find_requests(self):
        # Assuming self.parser.articles is a list of dictionaries
        self.requests = []
        for article in self.parser.articles:
            for paragraph in article['article_text']:
                if paragraph['eId'] in list(self.actions['eId']):                    
                    self.requests.append(
                        {
                            'eId': paragraph['eId'],
                            'request': paragraph['text']
                        }
                    )
        self.requests = pd.DataFrame(self.requests)

    def populate_graph(self):
        if 'uri' not in self.requests.columns:
            self.requests['request_uri'] = ''
            
        for index, request in self.requests.iterrows():
            # A request is the entire paragraph or the individual sentence?  
            request_uri = RRMV[f"Request{"{:02d}".format(index)}" + self.celex]
            self.requests.loc[index, 'request_uri'] = request_uri 
            # Add the paragraph as a request
            add_request(self.g, request_uri, request['request'], eli = self.celex , eId=request['eId'])  
        
        self.agents = []
        self.actions.reset_index().drop(columns='index')
        # Get corresponding request_uri by joining dataframes analyis.actions and analysis.requests on eId column.
        self.actions = self.actions.merge(self.requests, on='eId', how='left')
        
        agent_dict = []
        for index, action in self.actions.iterrows():
            action_uri = RRMV[f"Action{"{:02d}".format(index)}" + self.celex]
            self.actions.loc[index, 'action_uri'] = action_uri 
            add_action(self.g, action['sentence'], action['request_uri'], action_uri)
            # Action should be at least related to each other if belonging to the same request
            tree = DependencyTree(action['sentence'])

            # Adding agents
            agents = extract_agents(self.agents, tree)
            
            for agent_index, agent in enumerate(agents):
                agent_uri = ELI[f'Agent{agent_index}' + self.celex]
                tmp_agent_dict = {
                    "agent": agent,
                    'agent_uri': agent_uri        
                }
                agent_dict.append(tmp_agent_dict)    
                self.g = add_agent(self.g, agent_uri, agent)

            # Adding Action Results
            if tree.dobj is not None:
                action_result_uri = RRMV[f"ActionResult{index}" + self.celex]
                self.g = add_action_result(self.g, tree, action_result_uri, action_uri)
            # Adding PeriodOfTime
            if tree.date != '':
                period_uri = RRMV[f"PeriodOfTime{index}" + self.celex]
                self.g = add_date(self.g, tree, period_uri, action_uri)
            # Adding AgentRoles addresser
            if tree.subj_list is not None:
                for i, subj in enumerate(tree.subj_list):
                    addresser_uri = RRMV[f'Action{index}AgentRoleAddresser{i}' + self.celex]
                    self.g = add_agent_role(self.g, subj, addresser_uri, 'addresser', action_uri, agent_dict)
            if tree.pobj_list is not None:
                for i, pobj in enumerate(tree.pobj_list):
                    addressee_uri = RRMV[f'Action{index}AgentRoleAddressee{i}' + self.celex]
                    self.g = add_agent_role(self.g, pobj, addressee_uri, 'addressee', action_uri, agent_dict)
    
class DigitalRelevanceAnalysis(LegalAnalysis):
    def __init__(self):
        super().__init__()
        self.initialise_graph()

    def analyse(self, parser, celex):
        print('Launching analyisis of requirements of digital relevance')
        self.celex = celex
        self.parser = parser                
        self.get_sentences()
        #self.detect_requirements()
        #self.populate_graph()

    def get_sentences(self):        
        # Initialize an empty list to store data
        data = []
        self.paragraph = []
        for article in self.parser.articles:
            for paragraph in article['article_text']:
                paragraph['text'] = paragraph['text'].lstrip('0123456789.').strip()                
                data.append({
                            'celex': '32024R0903',
                            'eId': paragraph['eId'],
                            'paragraph': paragraph['text'].strip()
                        })
        self.paragraphs = pd.DataFrame(data)    

    def detect_requirements(self, file):        
        with open(file, 'r') as f:
            self.results = json.load(f)
        self.requirements = self.paragraphs
        self.requirements['digital_relevance'] = [req['digital_relevance'] for req in self.results if req['eId'] in self.requirements['eId'].values]
        self.requirements['digital_relevance'] = self.requirements['digital_relevance'].map({'1': True, '0': False})
    
    def populate_graph(self):
        if 'uri' not in self.requirement.columns:
            self.requirement['requirement_uri'] = ''
            
        for index, requirement in self.requirements.iterrows():
            # A request is the entire paragraph or the individual sentence?  
            requirement_uri = LFDS[f"Requirement{"{:02d}".format(index)}" + self.celex]
            self.requests.loc[index, 'requirement_uri'] = requirement_uri 
            # Add the paragraph as a request
            add_request(self.g, requirement_uri, requirement['request'], eli = self.celex , eId=requirement['eId'])  