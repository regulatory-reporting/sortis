import unittest
from sortis.parser import AkomaNtosoParser
from xml.dom import minidom

class TestIsTopLevel(unittest.TestCase):
    def setUp(self):
        self.parser = minidom.parseString("<root><content><p>Paragraph 1</p><p>Paragraph 2</p></content></root>")
        self.root = self.parser.documentElement
        self.content_node = self.root.getElementsByTagName("content")[0]
        self.p_node = self.content_node.getElementsByTagName("p")[0]

    def test_top_level_element(self):
        # Test a top-level element (direct child of the root node)
        self.assertTrue(AkomaNtosoParser().is_top_level(self.p_node))

    def test_not_top_level_element(self):
        # Test a non-top-level element (child of another element)
        self.assertFalse(AkomaNtosoParser().is_top_level(self.p_node))

    def test_nested_element(self):
        # Test an element nested multiple levels deep
        nested_node = self.p_node.appendChild(self.parser.createElement("div"))
        self.assertFalse(AkomaNtosoParser().is_top_level(nested_node))

    def test_root_node(self):
        # Test the root node itself
        self.assertTrue(AkomaNtosoParser().is_top_level(self.root))

    def test_node_with_parent_node_tagname_content(self):
        # Test an element with a parent node having tag name 'content'
        content_node = self.parser.createElement("content")
        p_node = self.parser.createElement("p")
        content_node.appendChild(p_node)
        self.assertTrue(AkomaNtosoParser().is_top_level(p_node))

    def test_node_with_parent_node_tagname_not_content(self):
        # Test an element with a parent node having tag name other than 'content'
        div_node = self.parser.createElement("div")
        p_node = self.parser.createElement("p")
        div_node.appendChild(p_node)
        self.assertFalse(AkomaNtosoParser().is_top_level(p_node))

if __name__ == '__main__':
    unittest.main()

