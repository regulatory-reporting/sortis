import unittest
from unittest.mock import patch
from sortis.classifier import SentenceClassifier

class TestSentenceClassifier(unittest.TestCase):
    def setUp(self):
        """Create a SentenceClassifier instance for each test."""
        self.sentence = "On the basis of the notifications from the Member States, the Commission shall publish a common catalogue of varieties."
        self.sentence_classifier = SentenceClassifier(self.sentence)

    def test_init_sets_sentence(self):
        """Test that the sentence is set correctly during initialization."""
        self.assertEqual(self.sentence_classifier.sentence, self.sentence)

    def test_find_root_verb(self):
        """Test that the root verb and the modal is found correctly."""
        self.assertEqual(self.sentence_classifier.root.text, 'publish')
        self.assertEqual(self.sentence_classifier.tree[self.sentence_classifier.modal_idx].text, 'shall')

    def test_reporting_requirement(self):
        """Test contains_reporting_requirement method with different sentences."""
        sentences = [
            ("On the basis of the notifications from the Member States, the Commission shall publish a common catalogue of varieties.", True),
            ("These Annexes may be amended in accordance with the procedure laid down in Article 17(2) in order to set additional or more stringent conditions for the certification of initial propagating material;", False),
            ("For the purpose of implementing Article 112, Member States may require manufacturers of immunological products to submit to a competent authority copies of all the control reports signed by the qualified person in accordance with Article 51.", False),
            ("The catalogue shall determine the principal morphological and physiological characters by which the varieties can be distinguished from one another.", False),
            ("The investigation officer shall investigate the alleged infringements, taking into account any comments submitted by the persons who are subject to the investigations, and shall submit a complete file with his findings to ESMA.", True),
            ("Not later than 2007-07-12, the Commission shall submit to the European Parliament and the Council a report on the advisability of establishing specific rules, including, where appropriate, positive lists, on categories of nutrients or of substances with a nutritional or physiological effect other than those referred to in paragraph 1, accompanied by any proposals for amendment to this Directive which the Commission deems necessary.", True),
        ]

        for sentence, expected_result in sentences:
            sentence_classifier = SentenceClassifier(sentence)
            self.assertEqual(sentence_classifier.contains_reporting_requirement(sentence_classifier.root), expected_result)

if __name__ == "__main__":
    unittest.main()