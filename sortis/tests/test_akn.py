
class TestAkomaNtosoParser(unittest.TestCase):

    def setUp(self):
        self.parser = AkomaNtosoParser()
        self.parser.celex = '01968L0193-20050714'

    def test_get_body(self):
        # Create a sample XML string
        

        # Test the get_body method
        self.parser.get_body('test.xml')
        self.assertIsNotNone(self.parser.body)

    def test_get_p_tags(self):
        # Create a sample XML string
        xml_string = '<akomaNtoso><body><p><p/></p><p/></body></akomaNtoso>'
        # Parse the XML string
        document = parseString(xml_string)
        # Save the XML file
        with open('test.xml', 'w') as f:
            document.writexml(f)
        # Test the get_body method
        self.parser.get_body('test.xml')
        # Test the get_p_tags method
        self.parser.get_p_tags()
        self.assertEqual(len(self.parser.p_elements), 2)

    def test_is_top_level(self):
        # Create a sample XML string
        xml_string = '<akomaNtoso><body><p><p/></p><p/></body></akomaNtoso>'
        # Parse the XML string
        document = parseString(xml_string)
        # Get the top-level p elements
        p_elements = document.getElementsByTagName('p')
        # Test the is_top_level method
        self.assertTrue(self.parser.is_top_level(p_elements[1]))
        self.assertFalse(self.parser.is_top_level(p_elements[0].getElementsByTagName('p')[0]))

    def test_extract_provisions(self):
        # Create a sample XML string
        xml_string = '<akomaNtoso><body><p> Sentence 1. Sentence 2.</p><p>Sentence 3.</p></body></akomaNtoso>'
        # Parse the XML string
        document = parseString(xml_string)
        # Save the XML file
        with open('test.xml', 'w') as f:
            document.writexml(f)
        # Test the get_body method
        self.parser.get_body('test.xml')
        # Test the get_p_tags method
        self.parser.get_p_tags()
        # Test the extract_provisions method
        provisions = self.parser.extract_provisions()
        self.assertEqual(len(provisions), 3)
        self.assertEqual(provisions[0]['sentence'], 'Sentence 1.')
        self.assertEqual(provisions[1]['sentence'], 'Sentence 2.')

    def test_process_element(self):
        # Create a sample XML string
        xml_string = '<p eId="eId"> Sentence 1. Sentence 2.</p>'
        # Parse the XML string
        document = parseString(xml_string)
        # Get the p element
        p_element = document.getElementsByTagName('p')[0]
        # Test the process_element method
        eId, sentences = self.parser.process_element(p_element)
        self.assertEqual(eId, 'eId')
        self.assertEqual(len(sentences), 2)

    def test_unmask_element(self):
        # Test the unmask_element method
        sentences = ['This is a sentence with a ref: REF1 and a date: DATE1.']
        refs = ['REF1: Original ref', 'DATE1: Original date']
        dates = ['DATE1: Original date']
        unmasked_sentences = self.parser.unmask_element(sentences, refs, dates)
        self.assertEqual(unmasked_sentences[0], 'This is a sentence with a ref: Original ref and a date: Original date.')

    def test_merge_dom_children(self):
        # Create a sample XML string
        xml_string = '<p><span>Sentence 1. </span><span>Sentence 2.</span></p>'
        # Parse the XML string
        document = parseString(xml_string)
        # Get the p element
        p_element = document.getElementsByTagName('p')[0]
        # Test the merge_dom_children method
        text = self.parser.merge_dom_children(p_element)
        self.assertEqual(text, 'Sentence 1. Sentence 2.')

    def test_process_text(self):
        # Test the process_text method
        text = 'Sentence 1. Sentence 2.'
        sentences = self.parser.process_text(text)
        self.assertEqual(len(sentences), 2)

    def test_get_eId(self):
        # Create a sample XML string
        xml_string = '<p eId="eId"> Sentence 1. Sentence 2.</p>'
        # Parse the XML string
        document = parseString(xml_string)
        # Get the p element
        p_element = document.getElementsByTagName('p')[0]
        # Test the get_eId method
        eId = self.parser.get_eId(p_element)
        self.assertEqual(eId, 'eId')

    def test_mask_element_by_tag(self):
        # Create a sample XML string
        xml_string = '<p><ref>Original ref</ref><date>Original date</date></p>'
        # Parse the XML string
        document = parseString(xml_string)
        # Get the p element
        p_element = document.getElementsByTagName('p')[0]
        # Test the mask_element_by_tag method
        p_element, refs = self.parser.mask_element_by_tag(p_element, 'ref', 'CELEX_NUMBER')
        p_element, dates = self.parser.mask_element_by_tag(p_element, 'date', 'CELEX_NUMBER')
        self.assertEqual(p_element.toxml(), '<p><ref> CELEX_NUMBER_ref00 </ref><date> CELEX_NUMBER_date00 </date></p>')

if __name__ == '__main__':
    unittest.main()