
# Contributing


The ```sortis``` repository is in active development, please follow EC's guidelines on open-source code and the following contributing guidelines.  

* Contributions can be made to the development branch, main is only for releases.
* Please make sure to sign all of your commits with a GPG key.
* Save data assets in a folder called `./data` containing the primary and transformated primary resources should be saved to the `./data` folder.
* The jupyter notebooks used during development should be saved in the `./notebook` folder
* The reference tables, statistical descriptions, other files should be saved in the  `./outputs` folder

## Installing a new package

If you need to install a new package, please make sure to also update the `pyproject.toml` file by installing it via poetry

```python
poetry add nameofthepackage
```

Please ONLY use license compatible with EUPL 1.2 or above.